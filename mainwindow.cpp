#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->pathToFile->setText("/home/zvebabi/study/data_co"); //standart path  lin: /home/zvebabi/study/data_co ;;; win: C:/data_co

    _init();

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::_init()
{
//Day before First Day of Sampling
    currentDay.day = 15;
    currentDay.month = "Nov";
    currentDay.year = 2012;

//Last Day of sampling
    lastDay.day = 30;
    lastDay.month = "Nov";
    lastDay.year = 2014;

//Months and days
    days["Jan"] = 31;
    days["Feb"] = 28;
    days["Mar"] = 31;
    days["Apr"] = 30;
    days["May"] = 31;
    days["Jun"] = 30;
    days["Jul"] = 31;
    days["Aug"] = 31;
    days["Sep"] = 30;
    days["Oct"] = 31;
    days["Nov"] = 30;
    days["Dec"] = 31;

//Name of months
    months <<"Jan" << "Feb" << "Mar"
           << "Apr" << "May" << "Jun"
           << "Jul" << "Aug" << "Sep"
           << "Oct" << "Nov" << "Dec";
}

void MainWindow::on_pushButton_clicked()
{
    QTime timer;
    timer.start();
    QString path = QString(ui->pathToFile->text());

//check for right end of path
    if (path.right(1) != "/")
        path.append(("/"));

//Write result here
    QString pathToResult = QString(path).append("result.csv");
    QFile result(pathToResult);
    if(!result.open(QIODevice::WriteOnly))
    {
        qDebug() << "Can't create result.csv";
        exit(-1);
    }
    QTextStream resultStream(&result);
    resultStream << "Date;" << "CO conc;" << "CO_error_se\n";

    while(currentDay != lastDay)
    {
        QString _path = QString(path).append(GetNextDay());
        double coDay=0; //mean value for day
        double coDayError =0;  //quadratic summ of error

        uint i = 0; //sample per day, one for each hour
        for(; i<24 ; ++i)
        {
            double coHour = 0; //mean value for hour
//file with data
            QString pathToFile = QString("%1%2").arg(_path).arg(i,2,10,QChar('0')).append(".txt");
            QFile dataFile(pathToFile);
            if(!dataFile.open(QIODevice::ReadOnly))
            {
                qDebug() << "Can't open file: " << pathToFile;
                continue;
            }
            QTextStream buffer(&dataFile);

            QStringList line;
            buffer.readLine();   //skip 1st line
            buffer.readLine();   //skip 2nd line

            uint j=0 ;  //sample per hour
            while (!buffer.atEnd())
            {
                line = buffer.readLine().split(",");
                if(line.size() < 3)   //each file have "-endline-" without comma
                    break;
                coHour += line.at(1).toFloat();
                coDayError += qPow(line.at(2).toFloat(), 2);
                ++j;
            }

            coHour /= j; //mean per hour
            coDay += coHour;

            dataFile.close();
        }

        coDay /= i; //mean for day
        coDayError = qSqrt(coDayError);

        QString oneDay = QString("%1;%2;%3\n").arg(_mainDay).arg(coDay).arg(coDayError);
        resultStream << oneDay;
        //qDebug() << _mainDay;
    }
    result.close();
    qDebug("Time elapsed: %d ms", timer.elapsed());
}

QString MainWindow::GetNextDay(void)
{
    //next day
    currentDay.day = (currentDay.day == days.value(currentDay.month)) ?
                1 : (currentDay.day+1) ;

    //next month
    if(currentDay.day == 1)
    {
        qint32 m = months.indexOf(currentDay.month)+1;
        if (m==12)
            m=0;
        currentDay.month = months.at(m);
    }

    //New Year
    if(currentDay.day == 1 && currentDay.month == "Jan")
        currentDay.year++;

    QString _d = QString("co_%1%2%3_f00").arg(currentDay.day,2,10,QChar('0')).arg(currentDay.month).arg(currentDay.year);
    _mainDay = QString("%1.%2.%3").arg(currentDay.day,2,10,QChar('0')).arg(months.indexOf(currentDay.month)+1).arg(currentDay.year);

    return _d;
}














