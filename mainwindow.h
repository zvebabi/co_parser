#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QString>
#include <QList>
#include <QStringList>
#include <QFile>
#include <QTextStream>
#include <QtMath>
#include <QDebug>
#include <QTime>
#include <memory>

using namespace std;

namespace Ui {
class MainWindow;
}

typedef class  {
public:
    qint32 day;
    QString month;
    qint32 year;
} sampleDate;

inline bool operator!= (sampleDate const &a, sampleDate const &b)
{
    return !(a.day==b.day && a.month==b.month && a.year==b.year);
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    sampleDate currentDay;
    sampleDate lastDay;
    QString _mainDay;  //excel-readable format
    QMap<QString,int> days;
    QStringList months;

    ~MainWindow();

private slots:
    void _init(void);
    void on_pushButton_clicked();
    QString GetNextDay(void);

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
